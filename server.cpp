// TODO: change project name to tcp-and-process-coroutines?

#include "process.hpp"
#include <asio.hpp>
#include <iostream>

using namespace std;

asio::io_context event_loop(1); // Create event_loop optimized for running in one thread

/// Awaitable function for running a process, and returning exit_status, standard output and standard error.
///
/// Students: do not expect to understand everything in this function.
auto async_process(std::string command, std::string standard_input = "") {
  return asio::async_compose<decltype(asio::use_awaitable), void(int, std::string, std::string)>(
      [command = std::move(command), standard_input = std::move(standard_input)](auto &&self) {
        std::thread([command = std::move(command), standard_input = std::move(standard_input), self = std::move(self)]() mutable {
          std::string standard_output, standard_error;

          TinyProcessLib::Process process(
              command, "",
              [&standard_output](const char *buffer, size_t length) {
                standard_output += std::string(buffer, length);
              },
              [&standard_error](const char *buffer, size_t length) {
                standard_error += std::string(buffer, length);
              },
              !standard_input.empty());
          if (!standard_input.empty()) {
            process.write(standard_input);
            process.close_stdin();
          }
          auto exit_status = process.get_exit_status();
          self.complete(exit_status, standard_output, standard_error);
        }).detach();
      },
      asio::use_awaitable);
}

asio::awaitable<void> server(asio::ip::tcp::socket socket) {
  while (true) {
    std::string command;
    // Returns number of bytes up to and including \r\n, but input_buffer can receive additional extra bytes
    co_await asio::async_read_until(socket, asio::dynamic_buffer(command), "\r\n", asio::use_awaitable);

    auto [exit_status, standard_output, standard_error] = co_await async_process(command.substr(0, command.size() - 2));

    co_await asio::async_write(socket, asio::buffer(standard_output), asio::use_awaitable);
  }
}

asio::awaitable<void> listener() {
  asio::ip::tcp::acceptor acceptor(event_loop, {asio::ip::tcp::v4(), 8080});
  for (;;) {
    auto socket = co_await acceptor.async_accept(asio::use_awaitable);
    co_spawn(event_loop, server(std::move(socket)), asio::detached);
  }
}

int main() {
  co_spawn(event_loop, listener, asio::detached);

  event_loop.run(); // Run event_loop in main thread
}
